package mx.leo.incodeproject.home.domain.model;

public class Photo {

  private String picture;
  private String publishedAt;
  private String comment;

  public String getPicture() {
    return picture;
  }

  public String getPublishedDate() {
    return publishedAt;
  }

  public String getComment() {
    return comment;
  }

}
