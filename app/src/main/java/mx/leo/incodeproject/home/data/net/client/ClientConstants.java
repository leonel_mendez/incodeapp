package mx.leo.incodeproject.home.data.net.client;

public class ClientConstants {
  public static class Endpoint {
    public static final String BASE_URL = "http://beta.json-generator.com/api/json/";
    public static final String PHOTOS_ENDPOINT = "get/EkphH5xyM";
  }
}
