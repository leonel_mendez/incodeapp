package mx.leo.incodeproject.home.domain.usecase;


import java.util.List;

import mx.leo.incodeproject.home.data.net.client.PhotoClient;
import mx.leo.incodeproject.home.domain.model.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotosUseCase {

  private UseCaseResponse useCaseResponse;

  public void setUseCaseResponse(
      UseCaseResponse useCaseResponse) {
    this.useCaseResponse = useCaseResponse;
  }

  public void execute() {
    PhotoClient.createClient().timelinePhotos().enqueue(new Callback<List<Photo>>() {
      @Override
      public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
        if (useCaseResponse != null) {
          if (response.isSuccessful()) {
            useCaseResponse.response(response.body());
          } else {
            useCaseResponse.failure(new Error("There was a problem getting photos"));
          }
        }
      }

      @Override
      public void onFailure(Call<List<Photo>> call, Throwable t) {
        if (useCaseResponse != null) {
          useCaseResponse.failure(new Error(t.getMessage()));
        }
      }
    });
  }

  public interface UseCaseResponse {
    void response(List<Photo> photos);

    void failure(Error error);
  }
}
