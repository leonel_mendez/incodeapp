package mx.leo.incodeproject.home.presentation;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import mx.leo.incodeproject.R;
import mx.leo.incodeproject.detail.PhotoDetailActivity;
import mx.leo.incodeproject.home.domain.model.Photo;
import mx.leo.incodeproject.home.domain.usecase.PhotosUseCase;

import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_COMMENT;
import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_DATE;
import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_URL;

class HomePresenter {

  private static final int CAMERA_REQUEST = 0x001;
  static final int REQUEST_CAMERA_PERMISSIONS = 0x010;
  private static final String FILE_IMAGE_EXTENSION = ".jpg";
  private static final String FILE_PROVIDER_AUTHORITY = "mx.leo.incodeproject.fileprovider";
  private static final String FILE_IMAGE_NAME = "incode_image_";
  private static final String INCODE_IMAGES_DIRECTORY = "incode-images-folder";

  private HomeView homeView;
  private PhotosUseCase photosUseCase;

  HomePresenter(HomeView homeView,
      PhotosUseCase photosUseCase) {
    this.homeView = homeView;
    this.photosUseCase = photosUseCase;
  }


  void getPhotos() {
    photosUseCase.execute();
    photosUseCase.setUseCaseResponse(new PhotosUseCase.UseCaseResponse() {

      @Override
      public void response(List<Photo> photos) {
        if (photos.size() > 0) {
          homeView.showPhotos(photos);
        } else {
          homeView.showEmptyMessage(R.string.empty_message);
        }

      }

      @Override
      public void failure(Error error) {
        homeView.showError(error.getMessage());
      }
    });
  }

  void openCamera(final Activity activity) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

      if (activity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
          && activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
          && activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

        if (activity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
            || activity.shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
            || activity.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {

        } else {
          activity.requestPermissions(new String[]{Manifest.permission.CAMERA
                  , Manifest.permission.READ_EXTERNAL_STORAGE
                  , Manifest.permission.WRITE_EXTERNAL_STORAGE},
              REQUEST_CAMERA_PERMISSIONS);
        }
      } else {
        createCameraIntent(activity);
      }
    } else {
      createCameraIntent(activity);
    }
  }

  private void createCameraIntent(Activity activity) {
    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    File photo = createImageFile();

    if (photo != null) {
      Uri photoUri = FileProvider.getUriForFile(activity, FILE_PROVIDER_AUTHORITY, photo);
      cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
      activity.startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

  }

  void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    if (requestCode == CAMERA_REQUEST) {
    }
  }

  private File createImageFile() {

    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = FILE_IMAGE_NAME + timeStamp + FILE_IMAGE_EXTENSION;


    File imagesDirectory = new File(Environment.getExternalStorageDirectory(), INCODE_IMAGES_DIRECTORY);


    if (!imagesDirectory.exists()) {
      imagesDirectory.mkdirs();
    }
    String photoPath = Environment.getExternalStorageDirectory()
        + File.separator
        + INCODE_IMAGES_DIRECTORY
        + File.separator
        + imageFileName;

    return new File(photoPath);


  }

  void showPhotoDetail(Activity activity, Photo photo) {
    Intent detailPhotoIntent = new Intent(activity, PhotoDetailActivity.class);
    detailPhotoIntent.putExtra(PICTURE_URL, photo.getPicture());
    detailPhotoIntent.putExtra(PICTURE_DATE, photo.getPublishedDate());
    detailPhotoIntent.putExtra(PICTURE_COMMENT, photo.getComment());
    activity.startActivity(detailPhotoIntent);
  }
}
