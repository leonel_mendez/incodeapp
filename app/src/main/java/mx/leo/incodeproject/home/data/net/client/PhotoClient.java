package mx.leo.incodeproject.home.data.net.client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotoClient {

  public static RetrofitPhotoService createClient() {
    return new Retrofit.Builder()
        .baseUrl(ClientConstants.Endpoint.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(RetrofitPhotoService.class);
  }
}
