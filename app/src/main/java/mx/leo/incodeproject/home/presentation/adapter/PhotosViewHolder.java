package mx.leo.incodeproject.home.presentation.adapter;

import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;

import mx.leo.easyrecycler.viewholder.EasyViewHolder;
import mx.leo.incodeproject.R;
import mx.leo.incodeproject.home.domain.model.Photo;


public class PhotosViewHolder extends EasyViewHolder {

  private SimpleDraweeView photo;

  public PhotosViewHolder(View view) {
    super(view);
    photo = (SimpleDraweeView) view.findViewById(R.id.photo_item);
  }

  public void bindView(Photo photoItem) {
    photo.setImageURI(photoItem.getPicture());
  }

}
