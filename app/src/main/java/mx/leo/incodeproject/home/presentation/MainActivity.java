package mx.leo.incodeproject.home.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mx.leo.easyrecycler.util.RecyclerViewItemClickListener;
import mx.leo.easyrecycler.util.extensions.RecyclerViewExtensionsKt;
import mx.leo.incodeproject.R;
import mx.leo.incodeproject.detail.PhotoDetailActivity;
import mx.leo.incodeproject.home.domain.model.Photo;
import mx.leo.incodeproject.home.domain.usecase.PhotosUseCase;
import mx.leo.incodeproject.home.presentation.adapter.PhotosAdapter;

import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_COMMENT;
import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_DATE;
import static mx.leo.incodeproject.detail.PhotoDetailActivity.PICTURE_URL;

public class MainActivity extends AppCompatActivity implements HomeView {

  private static final int RECYCLER_SPAN = 2;
  private PhotosAdapter photosAdapter;
  private HomePresenter homePresenter;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    PhotosUseCase photosUseCase = new PhotosUseCase();
    homePresenter = new HomePresenter(MainActivity.this, photosUseCase);
    homePresenter.getPhotos();
    configRecyclerView();

  }

  private void configRecyclerView() {
    final RecyclerView photoList = (RecyclerView) findViewById(R.id.photo_list);
    photosAdapter = new PhotosAdapter();
    photoList.setLayoutManager(new GridLayoutManager(this, RECYCLER_SPAN));
    photoList.setAdapter(photosAdapter);
    photoList.setHasFixedSize(true);

    RecyclerViewExtensionsKt.OnItemClickListener(photoList, new RecyclerViewItemClickListener.OnItemClickListener() {
      @Override
      public void onItemClick(View view, Integer position) {
        homePresenter.showPhotoDetail(MainActivity.this,photosAdapter.getItems().get(position));
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.home_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.menu_camera_item) {
      homePresenter.openCamera(MainActivity.this);
      return true;

    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == HomePresenter.REQUEST_CAMERA_PERMISSIONS) {
      homePresenter.openCamera(MainActivity.this);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    homePresenter.onActivityResult(MainActivity.this, requestCode, requestCode, data);
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void showPhotos(List<Photo> photos) {
    photosAdapter.addItems((ArrayList<Photo>) photos);
  }

  @Override
  public void showEmptyMessage(@StringRes int message) {
    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void showError(String message) {
    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
  }

}
