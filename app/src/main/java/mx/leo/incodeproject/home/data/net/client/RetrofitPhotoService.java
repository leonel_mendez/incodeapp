package mx.leo.incodeproject.home.data.net.client;

import java.util.List;

import mx.leo.incodeproject.home.domain.model.Photo;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitPhotoService {
  @GET(ClientConstants.Endpoint.PHOTOS_ENDPOINT)
  Call<List<Photo>> timelinePhotos();
}
