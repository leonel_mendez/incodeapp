package mx.leo.incodeproject.home.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import mx.leo.easyrecycler.adapter.EasyAdapter;
import mx.leo.incodeproject.R;
import mx.leo.incodeproject.home.domain.model.Photo;

public class PhotosAdapter extends EasyAdapter<PhotosViewHolder, Photo> {

  @NotNull
  @Override
  public PhotosViewHolder createHolder(ViewGroup viewGroup, int i) {
    return new PhotosViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.photo_item_layout, viewGroup, false));
  }

  @Override
  public void onBindItemViewHolder(PhotosViewHolder holder, Photo photo, int i) {
    holder.bindView(photo);
  }
}
