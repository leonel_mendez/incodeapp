package mx.leo.incodeproject.home.presentation;

import android.support.annotation.StringRes;

import java.util.List;

import mx.leo.incodeproject.home.domain.model.Photo;

public interface HomeView {
  void showPhotos(List<Photo> photos);

  void showEmptyMessage(@StringRes int message);

  void showError(String message);
}
