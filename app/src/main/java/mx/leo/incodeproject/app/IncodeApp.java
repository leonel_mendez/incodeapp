package mx.leo.incodeproject.app;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class IncodeApp extends Application {
  @Override
  public void onCreate() {
    Fresco.initialize(this);
    super.onCreate();
  }
}
