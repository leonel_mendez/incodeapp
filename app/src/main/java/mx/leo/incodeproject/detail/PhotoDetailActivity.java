package mx.leo.incodeproject.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import mx.leo.incodeproject.R;

public class PhotoDetailActivity extends AppCompatActivity {

  public static final String PICTURE_URL = "picture_url";
  public static final String PICTURE_DATE = "picture_date";
  public static final String PICTURE_COMMENT = "picture_comment";
  private static final String SHARE_IMAGE_TYPE = "image/*";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_photo_detail);
    showPhotoDetail();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.photo_detail_menu, menu);
    MenuItem menuItem = menu.findItem(R.id.menu_item_share);
    ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
    shareActionProvider.setShareIntent(createShareIntent());
    return true;
  }

  private void showPhotoDetail() {
    if (getIntent() != null) {
      SimpleDraweeView photoDetail = (SimpleDraweeView) findViewById(R.id.photo_detail);
      photoDetail.setImageURI(getIntent().getStringExtra(PICTURE_URL));
      TextView photoDetailDate = (TextView) findViewById(R.id.photo_detail_date);
      photoDetailDate.setText(getIntent().getStringExtra(PICTURE_DATE));
      TextView photoDetailComment = (TextView) findViewById(R.id.photo_detail_comment);
      photoDetailComment.setText(getIntent().getStringExtra(PICTURE_COMMENT));
    }
  }

  private Intent createShareIntent() {
    if (getIntent() != null) {
      Intent shareIntent = new Intent(Intent.ACTION_SEND);
      shareIntent.setType(SHARE_IMAGE_TYPE);
      shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(getIntent().getStringExtra(PICTURE_URL)));
      return shareIntent;
    }
    return null;
  }
}
